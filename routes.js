const express = require('express');
const response = require('./response');
const router = express.Router();

const gitlab = require('./services/gitlab');

const index = function (req, res) {

    response.res404(res);

}

router.all('/', index);

router.post('/commit', gitlab.commit);

router.all('*', index);

module.exports = router;
