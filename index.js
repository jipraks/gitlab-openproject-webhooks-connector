require('dotenv').config();

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morganBody = require('morgan-body');
const PORT = process.env.PORT || 4000;
const routes = require('./routes');

app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '5mb' }));

morganBody(app);

app.use(function (error, req, res, next) {
    if (error instanceof SyntaxError) {
        return res.status(500).send({ message: "Invalid data structure. Please read documentation carefully." });
    } else {
        next();
    }
});

app.use(routes);

app.disable('x-powered-by');

app.listen(PORT, () => console.log(`app listening on port ${PORT}`));