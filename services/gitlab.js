'use strict';

const response = require('../response');
const openproject = require('../libraries/openproject');
const logger = require('../libraries/logger');

exports.commit = function (req, res) {

    if (req.headers['x-gitlab-token'] === process.env.X_GITLAB_TOKEN) {

        logger.log('Commit triggered.');

        if (req.body.total_commits_count > 0) {

            logger.log("Found " + req.body.total_commits_count + " commit(s). Processing commit(s)");

            const commits = req.body.commits;

            commits.map((c) => {

                if (c.title.indexOf('#OP') >= 0) {

                    const titles = c.title.split(" ");
                    let wpId = null;

                    titles.map((t) => {

                        if (t.indexOf('#OP') >= 0) {

                            wpId = t.substr(3);

                        }

                    });

                    const opData = {

                        wpId: wpId,
                        commitUrl: c.url,
                        commitAuthor: c.author.name,
                        commitTitle: c.title,
                        projectId: req.body.project_id

                    };

                    logger.log("Found commit with #OP tag (WP #" + wpId + "). Processing to OpenProject.");
                    openproject.updateWorkPackage(opData);

                }

            });

        } else {

            logger.log("No commit found.")

        }

        response.res200(res);

    } else {

        response.res401(res);

    }

}