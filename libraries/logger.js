'use strict';

const moment = require('moment');

exports.log = function (data) {

    const curdate   = moment().format('YYYY-MM-DD h:mm:ss');
    const logs      = "[" + curdate + "] - " + data;
    console.log(logs);

}