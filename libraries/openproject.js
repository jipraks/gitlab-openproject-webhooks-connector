'use strict';

const fetch = require('node-fetch');
const logger = require('./logger');
const OP_MAIN_URL = process.env.OP_MAIN_URL;
const OP_API_KEY = process.env.OP_API_KEY;

exports.updateWorkPackage = function (data) {

    getWorkPackageData(data.wpId, function (wpData) {

        if (wpData) {

            if (process.env["PRJ_" + data.projectId] == wpData._embedded.project.identifier) {

                if (wpData._embedded.status.id !== 8) {

                    const lockVersion = wpData.lockVersion;
                    updateWpStatus(data.wpId, lockVersion, 'developed');
                    addCommentToWp(data.wpId, data);

                } else {

                    logger.log("WP #" + data.wpId + " status is DEVELOPED. Ignoring commit");

                }

            } else {

                logger.log("Commit not match with the project identifier. Ignoring.");

            }

        } else {

            logger.log("WP #" + data.wpId + "not found. Ignoring commit");

        }

    });

}

function getWorkPackageData(wpId, result) {

    fetch(OP_MAIN_URL + '/api/v3/work_packages/' + wpId, {
        headers: {
            Authorization: 'Basic ' + Buffer.from("apikey:" + OP_API_KEY).toString('base64')
        },
        method: 'GET'
    })
        .then((response) => {

            if (response.status === 200) {

                return response.json();

            } else {

                throw ("Work Package not found");

            }

        })
        .then((json) => {

            result(json);

        })
        .catch((error) => {

            logger.log(error);
            result(false);

        })

}

function updateWpStatus(wpId, lockVersion, status) {

    if (status === 'developed') {

        const body = {
            "lockVersion": lockVersion,
            "_links": {
                "status": {
                    "href": "/api/v3/statuses/8",
                    "title": "Developed"
                }
            }
        };

        fetch(OP_MAIN_URL + '/api/v3/work_packages/' + wpId, {
            headers: {
                Authorization: 'Basic ' + Buffer.from("apikey:" + OP_API_KEY).toString('base64'),
                'Content-Type': 'application/json'
            },
            method: 'PATCH',
            body: JSON.stringify(body)
        })
            .then((response) => {

                if (response.status === 200) {

                    return response.json();

                } else {

                    throw ("Work Package not found");

                }

            })
            .then((json) => {

                

            })
            .catch((error) => {

                logger.log(error);

            })

    }

}

function addCommentToWp(wpId, commitData) {

    const body = {
        comment: {
            format: "markdown",
            raw: "<p>Auto update from Gitlab commit by <b>" + commitData.commitAuthor + "</b>.<br /><br />Commit message : <b>" + commitData.commitTitle + "</b><br />Commit URL : " + commitData.commitUrl + "</p>",
            html: "<p>Auto update from Gitlab commit by <b>" + commitData.commitAuthor + "</b>.<br /><br />Commit message : <b>" + commitData.commitTitle + "</b><br />Commit URL : " + commitData.commitUrl + "</p>"
        }
    }

    fetch(OP_MAIN_URL + '/api/v3/work_packages/' + wpId + '/activities', {
        headers: {
            Authorization: 'Basic ' + Buffer.from("apikey:" + OP_API_KEY).toString('base64'),
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(body)
    })
        .then((response) => {

            if (response.status === 201) {

                return response.json();

            } else {

                throw ("Add comment failed");

            }

        })
        .then((json) => {


        })
        .catch((error) => {

            logger.log(error);

        })


}